# WASI & Rust

```bash
rustup target add wasm32-wasi # try to add it to the container
cargo new --bin hello-app
cd hello-app
cargo build --target wasm32-wasi

wasmer target/wasm32-wasi/debug/hello-app.wasm good morning Bob
```
